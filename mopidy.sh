#!/bin/bash

# Copy config if it does not already exist
if [ ! -f /config/mopidy.conf ]; then
    cp /mopidy_default.conf /config/mopidy.conf
fi

exec mopidy --config /config/mopidy.conf
