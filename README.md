
# Docker Image for Mopidy

Docker image providing a Mopidy server with support for local files and
Jellyfin.

Based on https://github.com/IVData/dockerfiles/tree/master/mopidy.
