FROM debian:buster-slim
ENV DEBIAN_FRONTEND=noninteractive

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    wget \
    ca-certificates \
    gnupg && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/local/share/keyrings && \
    wget -q -O /usr/local/share/keyrings/mopidy-archive-keyring.gpg https://apt.mopidy.com/mopidy.gpg && \
    wget -q -O /etc/apt/sources.list.d/mopidy.list https://apt.mopidy.com/buster.list

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        mopidy \
        mopidy-mpd \
        mopidy-local \
        build-essential \
        python3-dev \
        python3-pip \
        python3-setuptools && \
    rm -rf /var/lib/apt/lists/*

RUN pip3 install --no-cache-dir \
        Mopidy-Jellyfin \
        Mopidy-Muse \
        Mopidy-Mobile

RUN mkdir -p /config && chown -R mopidy /config
COPY mopidy.conf /mopidy_default.conf
COPY mopidy.sh /usr/local/bin/mopidy.sh
RUN chmod +x /usr/local/bin/mopidy.sh

EXPOSE 6600 6680
USER mopidy
ENTRYPOINT ["/usr/local/bin/mopidy.sh"]
